<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name','blog_post_id','is_checked'];

    public function blogpost()
    {
        return $this->belongsTo(BlogPost::class);
    }
}
