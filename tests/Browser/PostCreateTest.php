<?php

namespace Tests\Browser;

use App\Models\BlogPost;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;


class PostCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function testExample()
    {
        //$user = User::factory('App\User')->create();
        $blogPost = BlogPost::factory('App\BlogPosts')->create();
        //$this->actingAs($user);

        $this->browse(function (Browser $browser) use ($blogPost) {
            $browser->visit('/login')
                    ->value('#email', 'unit@test.rs')
                    ->value('#password', 'unittest')
                    ->click('@login')
                    ->assertPathIs('/blog')
                    ->click('@createPost')
                    ->assertPathIs('/blog/create/post')
                    ->value('#title', $blogPost->title)
                    ->value('#body', $blogPost->body)
                    ->value('#ListInput', 'list')
                    ->click('@addTask')
                    ->value('#ListInput', 'list2')
                    ->click('@addTask')
                    ->check('#CheckBox-199')
                    //->click('#task_delete-198')
                    ->click('@submitButton')
                    ->dump();
                //
        });
    }

    private function addTasks(Browser $browser, $taskcount)
    {
        for($count = 0; $count < $taskcount; $count++)
        {
        }
    }
}
