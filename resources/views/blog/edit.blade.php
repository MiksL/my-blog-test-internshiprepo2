@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="/blog" class="btn ButtonCustom btn-m mt-3">Go back</a>
                <div class="border rounded mt-4 p-4">
                <div class="px-3 py-2 row">
                        <h1 class="display-4 w-75">Edit post</h1>
                        <button type="button" id="confirmPosition" class="btn ButtonCustom btn-m mt-3 mb-3 mx-auto text-center" onclick="savetaskvalues()">
                            Submit changes
                        </button>
                    </div>
                    <div class="px-3 py-2 row">
                        <p>Edit and submit this form to update a post</p>
                    </div>
                    <hr>

                    <form action="" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="control-group col-12">
                                        <label class="h4 mt-2" for="title">Post Title</label>
                                        <input type="text" id="title" class="form-control FormCustom" name="title" maxlength="64"
                                            placeholder="Enter Post Title" value="{{ $post->title }}" required>
                                    </div>
    
                                    <div class="control-group col-12 mt-2 mb-4">
                                        <label class="h4 mt-2" for="body">Post Body</label>
                                        <textarea id="body" class="form-control FormCustom" name="body" placeholder="Enter Post Body"
                                        rows="4" required>{{ $post->body }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-6 card-body p-2 px-4">
                                <h2 class="h4 mb-2">Tasks</h2>
                                <ul id="ListGroupElement" class="list-group">
                                    @forelse($tasks as $task)

                                        <li class="LiElementCustom row mx-1 mt-2 rounded">
                                            <div class=" col-1 d-flex justify-content-start p-0">
                                                <button name="checkboxbutton" value="{{ $task->is_checked }}" type="button" id="CheckBox{{$task->id}}" onclick="checkboxFunction({{$task->id}})"
                                                class="CheckButtonCustom my-auto ml-1 ButtonCustom"> </button>
                                            </div>
                                            <div class="col-9 p-2 px-2 d-flex justify-content-start align-items-center">
                                            <input maxlength="43" name="task_name[]" class="px-2 ButtonCustom w-100 h-100 TaskName" type="text" id="task_input{{$task->id}}" value={{$task->name}} placeholder="Type something..." required>
                                                <input type="hidden" name="task_id[]" id="task_id" value="{{ $task->id }}" >
                                                <input type="hidden" name="delete_task[]" id="delete_task{{ $task->id }}" value="0" >
                                                <input type="hidden" name="checkbox[]" id="checkboxvalue{{ $task->id }}" value="{{ $task->is_checked }}" >
                                            </div>
                                            <div class="col-2 py-auto d-flex justify-content-end p-2">
                                                <button type="button" id="task_delete{{ $task->id }}" value="0" onclick="deleteColorChange({{$task->id}});deleteTask( {{ $task->id }})" class="DeleteButtonCustom ButtonCustom w-100">Delete</button>
                                            </div>
                                        </li>
                                    @empty
                                    @endforelse
                                </ul>
                                <div class="text-center">
                                    <input class="FormCustom InputFormCustom my-3 px-2" type="text" id="ListInput" placeholder="Type something...">
                                    <button id="AddItemButton" onclick="AddListItems();" type="reset" class="btn ButtonCustom btn-m my-3">
                                            Add item
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div id="map" class="mt-4"></div>

                        <script>
                            updatetaskvalues(); // Calls the task colour updater on page load
                            function updatetaskvalues() // Updates task colors based on their is_checked value in the database
                            {
                                var tasks = document.getElementsByName("checkboxbutton"); // Selects all the checkbox elements (each task has 1 checkbox)
                                var taskCount = tasks.length; // Counts the amount of tasks (checkboxes) on the page
                                for(id = 0; id < taskCount; id++) // Loops through all the tasks
                                { // Assigns values for tasks depending on their is_checked value from the database (1 = green, 0 = default)
                                    var task = tasks[id];
                                    if (task.value == 1)
                                    {
                                        task.parentNode.parentNode.style.background = 'var(--success-green-l)';
                                        task.style.background = 'var(--success-green)';
                                    }
                                    else
                                    {
                                        task.parentNode.parentNode.style.background = 'rgba(0,0,0,0)';
                                        task.style.background = 'var(--button-bg)';
                                    }
                                }
                            }

                            function deleteTask(id) // Schedules the existing selected task to be deleted after changes are submitted and cancels deletion if pressed again
                            {
                                var selectedtask = document.getElementById("delete_task" + id );
                                if (selectedtask.value == 0) selectedtask.value = 1;
                                else selectedtask.value = 0;
                            }

                            var ItemCount = -200; // Newly created task temporary id starting point, used for deletion and editing of new tasks.
                            function AddListItems() // Adds the newly created task in the edit view and updates existing task names
                            {
                                document.getElementById("title").setAttribute('value',document.getElementById("title").value); // Updates the post title text
                                document.getElementById("body").innerText = document.getElementById("body").value; // Updates the post body text
                                
                                var tasks = document.getElementsByClassName("TaskName"); // Finds all the currently existing and newly created tasks
                                for(id = 0; id < tasks.length; id++) // Loops through all the tasks
                                {
                                    tasks[id].setAttribute('value',tasks[id].value); // Updates task names in the task name input fields
                                }

                                var ElementBox = document.getElementById("ListGroupElement") // Gets the main task list element

                                ElementBox.innerHTML += `
                                <li class="LiElementCustom row mx-1 mt-2 rounded">
                                    <div class=" col-1 d-flex justify-content-start p-0">
                                        <button name="checkbox[]" value="0" type="button" id="CheckBox`+ItemCount+`" onclick="checkboxFunction(`+ ItemCount +`)"
                                        class="CheckButtonCustom my-auto ml-1 ButtonCustom"> </button>
                                    </div>
                                    <div class="col-9 p-2 px-2 d-flex justify-content-start align-items-center">
                                    <input maxlength="43" name="task_name[]" class="px-2 ButtonCustom w-100 h-100 TaskName" type="text" id="task_input`+ ItemCount +`" value="" placeholder="Type something..." required>
                                    <input type="hidden" name="task_id[]" id="task_id" value="" >
                                    <input type="hidden" name="delete_task[]" id="delete_task" value="0" >
                                    <input type="hidden" name="checkbox[]" id="checkboxvalue`+ItemCount+`" value="0" >
                                    </div>
                                    <div class="col-2 py-auto d-flex justify-content-end p-2">
                                        <button type="button" id="task_delete`+ItemCount+`" value="0" onclick="deleteColorChange(`+ ItemCount +`);RemoveListItems(`+ ItemCount +`)" class="DeleteButtonCustom ButtonCustom w-100">Delete</button>
                                    </div>
                                </li>
                                `; // Inserts the task into the main list element
                                document.getElementById("task_input"+ItemCount).setAttribute('value',document.getElementById("ListInput").value); // Inserts the task name into the newly created element
                                ++ItemCount; // Increments the new task temporary id
                            }   

                            function RemoveListItems(id) // Removes the newly created task, thus removing it from being added to the database.
                            {
                                var Element = document.getElementById("CheckBox" + id); // Finds the appropriate task to be deleted
                                Element.parentNode.parentNode.remove(); // Removes the task
                            }

                            function checkboxFunction(id) // Alters the visual colours and hidden values of the task if checked or unchecked
                            {
                                var checkvalue = document.getElementById('checkboxvalue' + id);
                                var elem = document.getElementById("CheckBox" + id );
                                var delCheck = document.getElementById("task_delete" + id );
                                // Finds the required elements for the chosen task

                                if(delCheck.value == 0) // Checks if the task has not been schedule for deletion
                                {
                                    if (elem.value == 0) // Checks if the checkbox is not checked
                                    {
                                        elem.value = 1;
                                        checkvalue.value = 1;
                                        // Changes the checkbox and hidden input field values
                                        elem.parentNode.parentNode.style.background = 'var(--success-green-l)';
                                        elem.style.background = 'var(--success-green)';
                                        // Changes the colour of the checkbox and the task list element to green
                                    }
                                    else
                                    {
                                        elem.value = 0;
                                        checkvalue.value = 0;
                                        // Changes the checkbox and hidden input field values back to their defaults
                                        elem.parentNode.parentNode.style.background = 'rgba(0,0,0,0)';
                                        elem.style.background = 'var(--button-bg)';
                                        // Changes the colour of the checkbox and the task list element back to white
                                    }
                                }
                            }

                            function deleteColorChange(id) // Changes the colour of the task when the delete button is pressed
                            {
                                var deleteButton = document.getElementById("task_delete" + id ); // Finds the delete button associated with the appropriate task

                                if (deleteButton.value == 0) // Checks if the delete button has not been pressed before
                                {
                                    deleteButton.value = 1; // Sets the delete button value to indicate that it has bene pressed

                                    document.getElementById("CheckBox" + id).style.background = 'var(--wrong-orange-l)';
                                    deleteButton.parentNode.parentNode.querySelector('input').style.background = 'var(--wrong-orange-l)';
                                    deleteButton.parentNode.parentNode.style.background = 'var(--wrong-orange-l)';
                                    deleteButton.style.background = 'var(--wrong-orange)';
                                    // Changes the color of the task to represent that it is scheduled for deletion
                                }
                                else
                                {
                                    if(document.getElementById("CheckBox" + id ).value == 0) // Checks if the checkbox has not been pressed
                                    {
                                        deleteButton.parentNode.parentNode.style.background = 'rgba(0,0,0,0)';
                                        document.getElementById("CheckBox" + id).style.background = 'var(--button-bg)';
                                        deleteButton.parentNode.parentNode.querySelector('input').style.background = 'var(--button-bg)';
                                        deleteButton.style.background = 'var(--button-bg)';
                                        // Changes the colours of the task back to default
                                    }
                                    else
                                    {
                                        document.getElementById("CheckBox" + id).style.background = 'var(--success-green)';
                                        deleteButton.parentNode.parentNode.querySelector('input').style.background = 'var(--button-bg)';
                                        deleteButton.parentNode.parentNode.style.background = 'var(--success-green-l)';
                                        deleteButton.style.background = 'var(--button-bg)';
                                        // Changes the colours of the task back to green (as it was checked before being scheduled for deletion)
                                    }
                                    deleteButton.value = 0; // Sets the delete button value back to its default state
                                }
                            } 

                            // Map initialization code

                            var confirmBtn = document.getElementById('confirmPosition');
                            var onClickPositionView = document.getElementById('onClickPositionView');
                            var onIdlePositionView = document.getElementById('onIdlePositionView');
                    
                            var lp = new locationPicker('map', {
                                setCurrentPosition: true,
                            }, {
                                zoom: 10,
                                center: { lat: {{ $post->latitude }}, lng: {{ $post->longitude }} }
                            });
                    
                            confirmBtn.onclick = function () {
                                var location = lp.getMarkerPosition();
                                document.getElementById("latitude").value = location.lat;
                                document.getElementById("longitude").value = location.lng;
                                document.getElementById("btn-submit").click();
                            };
                        </script>



                        <div style="display:none;" class="row mt-2">
                            <div class="control-group col-12 text-center">
                                <button id="btn-submit" class="btn btn-outline-info btn-m mt-3 mb-3">
                                </button>
                            </div>
                        </div>
                        
                        <input type="hidden" name="latitude" id="latitude" value="{{ $post->latitude }}" >
                        <input type="hidden" name="longitude" id="longitude" value="{{ $post->longitude }}" >
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
